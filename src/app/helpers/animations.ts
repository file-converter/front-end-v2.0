import { trigger, transition, style, animate } from '@angular/animations';

const basicAnimations = {
	fadeEnter: transition(':enter', [
		style({ opacity: 0 }),
		animate('{{ durationMS }}ms {{ delayMS }}ms ease-in-out', style({ opacity: 1 }))
	], { params: { durationMS: 300, delayMS: 0 } }),
	fadeLeave: transition(':leave', [
		style({ opacity: 1 }),
		animate('{{ durationMS }}ms {{ delayMS }}ms ease-in-out', style({ opacity: 0 }))
	], { params: { durationMS: 300, delayMS: 0 } })
};

export const ANIMATIONS = {
	fade: trigger('fade', [basicAnimations.fadeEnter, basicAnimations.fadeLeave])
};
