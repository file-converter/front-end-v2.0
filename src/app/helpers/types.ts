export interface IFormat {
	id: string;
	name: string;
	type: string;
}

export enum IConversionProcess {
	IDLE = 0,
	PROCESSING = 1,
	DONE = 2,
	AVAILABLE = 3
}
