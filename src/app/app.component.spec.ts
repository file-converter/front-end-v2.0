import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpClient, HttpHandler, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BackEndService } from './services/back-end.service';
import { IFormat } from './helpers/types';
import { Observable } from 'rxjs';
import { SelectionMenuDirective } from './directives/selection-menu.directive';

const MOCK_UUID = '6c7fe122-3e3b-11ea-8d2b-bb63141ca3f2';
const MOCK_BLOB = new Blob([''], { type: 'image/png' });
const MOCK_FILE = new File([MOCK_BLOB], 'mock.png');
const ALL_FORMATS = [
	{
		id: '', name: 'JPG', type: 'image/jpeg'
	},
	{
		id: '', name: 'PNG', type: 'image/png'
	},
	{
		id: '', name: 'BMP', type: 'image/bmp'
	}
];
class MockBackEndService {
	getFormats(): Observable<IFormat[]> {
		return Observable.create(observer => {
			observer.next(ALL_FORMATS);
			observer.complete();
		});
	}

	getBlob(uuid: string): Observable<Blob> {
		return Observable.create(observer => {
			observer.next(new Blob([''], { type: 'image/png' }));
			observer.complete();
		});
	}

	saveFile() { }

	convertFile(): Observable<string> {
		return Observable.create(observer => {
			observer.next(MOCK_FILE);
			observer.complete();
		});
	}

	selectAvailableFormats(): IFormat[] {
		return ALL_FORMATS.slice(1, ALL_FORMATS.length);
	}
}

describe('AppComponent', () => {
	let fixture;
	let app;
	let service;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [
				AppComponent,
				SelectionMenuDirective
			],
			imports: [HttpClientModule, HttpClientTestingModule, BrowserAnimationsModule],
			providers: [
				{ provide: BackEndService, useClass: MockBackEndService }
			]
		}).compileComponents();
		fixture = TestBed.createComponent(AppComponent);
		app = fixture.componentInstance;
		service = TestBed.get(BackEndService);
		fixture.detectChanges();
	}));

	it('should create the app', () => {
		expect(app).toBeTruthy();
	});

	it('should initialize all formats', async () => {
		spyOn(service, 'getFormats').and.returnValue(Observable.create(observer => {
			observer.next(ALL_FORMATS);
			observer.complete();
		}));

		await app.ngOnInit();
		expect(service.getFormats).toHaveBeenCalled();
		expect(app.allFormats).toEqual(ALL_FORMATS);
	});

	it('should select a file', () => {
		spyOn(service, 'selectAvailableFormats');
		app.selectFile(MOCK_FILE);

		expect(app.selectedFile).toBeTruthy();
		expect(service.selectAvailableFormats).toHaveBeenCalled();
	});

	it('should select a format', () => {
		app.selectFormat(ALL_FORMATS[0]);
		expect(app.selectedFormat).toEqual(ALL_FORMATS[0]);
	});

	it('should convert a file', async () => {
		spyOn(service, 'convertFile').and.returnValue(Observable.create(observer => {
			observer.next(MOCK_UUID);
			observer.complete();
		}));

		app.selectFile(MOCK_FILE);
		app.selectFormat(ALL_FORMATS[0]);
		await app.convertFile();

		expect(service.convertFile).toHaveBeenCalled();
		setTimeout(() => expect(app.conversionUUID).toEqual(MOCK_UUID), 750);
	});

	it('should download a file', async () => {
		spyOn(service, 'saveFile');

		app.selectFile(MOCK_FILE);
		app.selectFormat(ALL_FORMATS[0]);
		await app.convertFile();
		app.downloadFile();

		expect(service.saveFile).toHaveBeenCalled();
	});
});
