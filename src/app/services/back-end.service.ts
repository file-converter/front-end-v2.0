import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IFormat } from '../helpers/types';
import { Observable } from 'rxjs';
import { FileSaverService } from 'ngx-filesaver';

@Injectable({
	providedIn: 'root'
})
export class BackEndService {
	public apiUrl = 'http://localhost:8080/api';

	constructor(
		private http: HttpClient,
		private fileSaverService: FileSaverService
	) { }

	/**
	 * @description Gets all available formats from the API
	 * @returns an observable of all available formats
	 */
	getFormats(): Observable<IFormat[]> {
		return Observable.create(observer => {
			this.http.get(`${this.apiUrl}/formats`).subscribe((formats: IFormat[]) => {
				observer.next(formats);
				observer.complete();
			}, (err) => {
				console.log(err);
			});
		});
	}

	/**
	 * @description Gets the converted file data by its corresponding uuid
	 * @param uuid of the file
	 * @returns an observable blob or an observable boolean
		*/
	getBlob(uuid: string): Observable<Blob> {
		return Observable.create(observer => {
			this.http.get(`${this.apiUrl}/converter/file`, {
				responseType: 'blob',
				headers: new HttpHeaders({
					uuid
				})
			}).subscribe(blob => {
				observer.next(blob);
				observer.complete();
			});
		});
	}

	/**
	 * @description Gets a converted file and then saves it to the local filesystem
	 * @param uuid of the file
	 * @param type of the file
	 * @param mime of the file
	 */
	saveFile(uuid: string, type: string, mime: string) {
		this.getBlob(uuid).subscribe(blob => {
			const file = new File([blob], `${uuid}.${type}`, { type: mime });
			this.fileSaverService.save(file, `${uuid}.${type}`, mime);
		});
	}

	/**
	 * @description Converts the selected file to a new format
	 * @param file that has been selected for conversion
	 * @param newFormat to be converted to
	 * @returns an observable of the uuid of the converted file
	 */
	convertFile(file: File, newFormat: IFormat): Observable<string> {
		return Observable.create(observer => {
			const formData: FormData = new FormData();
			formData.append('file', file, file.name);
			this.http.post(`${this.apiUrl}/converter/${newFormat.name.toLowerCase()}`, formData, {
				responseType: 'text'
			}).subscribe((uuid) => {
				observer.next(uuid);
				observer.complete();
			});
		});
	}

	/**
	 * @description Selectes available formats for conversion based on the format of the
	 * selected file, if a file is an image, selects all mime types that start with 'image/'
	 * if a file is a video, selects all mime types that start with 'video/'
	 * @param allFormats that are available
	 * @param currentFormat of the selected file
	 * @returns a format model array
	 */
	selectAvailableFormats(allFormats: IFormat[], currentFormat: string): IFormat[] {
		const availableFormats: IFormat[] = [];
		const currentFormatMedium = currentFormat.split('/')[0];

		for (const format of allFormats) {
			const formatMedium = format.type.split('/')[0];
			if (formatMedium === currentFormatMedium && format.type !== currentFormat) {
				availableFormats.push(format);
			}
		}

		return availableFormats;
	}
}
