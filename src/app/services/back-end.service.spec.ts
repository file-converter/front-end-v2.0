import { TestBed } from '@angular/core/testing';

import { BackEndService } from './back-end.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { IFormat } from '../helpers/types';
import { HttpClientModule } from '@angular/common/http';
import { FileSaverService } from 'ngx-filesaver';

describe('BackEndService', () => {
	let service: BackEndService;
	let fileSaverService: FileSaverService;
	let httpMock: HttpTestingController;
	const MOCK_BLOB = new Blob([''], { type: 'image/png' });
	const MOCK_UUID = '6c7fe122-3e3b-11ea-8d2b-bb63141ca3f2';
	const ALL_FORMATS: IFormat[] = [
		{
			id: '', name: 'JPG', type: 'image/jpeg'
		},
		{
			id: '', name: 'PNG', type: 'image/png'
		},
		{
			id: '', name: 'BMP', type: 'image/bmp'
		}
	];

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientModule, HttpClientTestingModule],
			providers: [BackEndService]
		});
		service = TestBed.get(BackEndService);
		fileSaverService = new FileSaverService();
		httpMock = TestBed.get(HttpTestingController);
	});

	afterEach(() => {
		httpMock.verify();
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should get all formats', () => {
		service.getFormats().subscribe(formats => {
			expect(formats.length).toBe(3);
			expect(formats).toEqual(ALL_FORMATS);
		});
		const request = httpMock.expectOne(`${service.apiUrl}/formats`);
		expect(request.request.method).toBe('GET');
		request.flush(ALL_FORMATS);
	});

	it('should get a blob', () => {
		service.getBlob(MOCK_UUID).subscribe(blob => {
			expect(blob).toBeInstanceOf(Blob);
		});
		const request = httpMock.expectOne(`${service.apiUrl}/converter/file`);
		expect(request.request.method).toBe('GET');
		request.flush(MOCK_BLOB);
	});

	it('should save a file', async () => {
		spyOn(fileSaverService, 'save');
		service.saveFile(MOCK_UUID, 'png', 'image/png');
		const request = httpMock.expectOne(`${service.apiUrl}/converter/file`);
		expect(request.request.method).toBe('GET');
		setTimeout(() => {
			expect(fileSaverService.save).toHaveBeenCalled();
		});
	});

	it('should convert a file', () => {
		const mockFile = new File([''], 'test.png', { type: 'image/png' });
		service.convertFile(mockFile, ALL_FORMATS[0]).subscribe(uuid => {
			expect(uuid.length).toBe(36);
			expect(uuid).toEqual(MOCK_UUID);
		});
		const request = httpMock.expectOne(`${service.apiUrl}/converter/jpg`);
		expect(request.request.method).toBe('POST');
		request.flush(MOCK_UUID);
	});

	it('should select available formats', () => {
		const currentFormat = ALL_FORMATS[0];
		const availableFormats = service.selectAvailableFormats(ALL_FORMATS, currentFormat.type);

		for (const format of availableFormats) {
			expect(format.type.split('/')[0]).toMatch('image');
		}
	});
});
