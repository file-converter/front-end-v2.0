import { SelectionMenuDirective } from './selection-menu.directive';
import { Component, ElementRef, Renderer2, EventEmitter } from '@angular/core';
import { IFormat } from '../helpers/types';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'test-component',
	template: `
		<main>
			<input id="file-upload"/>
			<button
				id="format-select"
				type="button"
				[appSelectionMenu]="availableFormats"
				(selectFormat)="selectFormat($event)">
				SELECT FORMAT
			</button>
			<button id="file-convert">CONVERT</button>
		</main>
	`,
	styles: [`
		.selection-menu {
			position: absolute;
			z-index: 1000;
			list-style-type: none;
			width: 150px;
			background-color: $btn-color;
			padding: 0px;
			font-weight: 600;
			font-size: $btn-font-size;
			transition: .3s ease;
			opacity: 0;

			&-visible {
				opacity: 1;
			}

			&-item {
				height: 35px;
				text-align: center;
				line-height: 35px;
				font-size: 1rem;
				user-select: none;
				transition: .3s ease;
				cursor: pointer;

				&:hover {
					background-color: $btn-color-hover;
				}

				&:active {
					background-color: $btn-color-active;
				}
			}

			&-item:not(:last-child) {
				border-bottom: 1px solid darken($btn-color-active, 12.5%);
			}

			&:after {
				content: "";
				position: absolute;
				border-style: solid;
				top: -57%;
				left: 50%;
				margin-left: -20px;
				border-width: 20px;
				border-color: transparent transparent $btn-color transparent;
			}
		}
	`]
})
class TestComponent {
	availableFormats: IFormat[] = [
		{
			id: '6c7fe122-3e3b-11ea-8d2b-bb63141ca3f2', name: 'PNG', type: 'image/png'
		},
		{
			id: '6c7ff9b4-3e3b-11ea-8d2b-937e81e123fb', name: 'BMP', type: 'image/bmp'
		}
	];

	constructor(private el: ElementRef, private renderer: Renderer2) { }

	selectFormat(format: IFormat) {

	}
}

describe('SelectionMenuDirective', () => {
	let component: TestComponent;
	let fixture: ComponentFixture<TestComponent>;
	let renderer2: Renderer2;
	let directive: SelectionMenuDirective;
	let btnEl;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [TestComponent, SelectionMenuDirective],
			providers: [Renderer2]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(TestComponent);
		component = fixture.componentInstance;
		btnEl = fixture.debugElement.query(By.css('#format-select'));
		renderer2 = fixture.componentRef.injector.get(Renderer2);
		directive = new SelectionMenuDirective(fixture.nativeElement, renderer2);

		fixture.detectChanges();
	});

	it('should create an instance', () => {
		expect(component).toBeTruthy();
		expect(directive).toBeTruthy();
	});

	it('should have an initialized input availableFormats', () => {
		expect(component.availableFormats).not.toBeNull();
		expect(component.availableFormats.length).toBe(2);
	});

	it('should toggle selection menu', async () => {
		spyOn(renderer2, 'appendChild');
		spyOn(renderer2, 'removeChild');

		btnEl.nativeElement.dispatchEvent(new Event('click'));
		await fixture.whenStable();
		expect(renderer2.appendChild).toHaveBeenCalled();

		btnEl.nativeElement.dispatchEvent(new Event('click'));
		await fixture.whenStable();
		expect(renderer2.removeChild).toHaveBeenCalled();
	});

	it('should emit the selected format', async () => {
		btnEl.nativeElement.dispatchEvent(new Event('click'));
		await fixture.whenStable();

		const menuEl = fixture.debugElement.query(By.css('ul'));
		menuEl.children[0].nativeElement.dispatchEvent(new Event('click'));
		await fixture.whenStable();

		expect(directive.selectFormat).toBeInstanceOf(EventEmitter);
		directive.selectFormat.subscribe(format => {
			expect(format.name).toMatch('PNG');
		});
	});

	it('should set the position of the menu', () => {
		spyOn(renderer2, 'setStyle');
		btnEl.nativeElement.dispatchEvent(new Event('click'));
		expect(renderer2.setStyle).toHaveBeenCalled();
	});
});
