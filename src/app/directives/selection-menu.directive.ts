import { Directive, Input, Output, EventEmitter, Renderer2, HostListener, ElementRef } from '@angular/core';
import { IFormat } from '../helpers/types';

@Directive({
	selector: '[appSelectionMenu]'
})
export class SelectionMenuDirective {
	@Input() appSelectionMenu: IFormat[];
	@Output() public selectFormat = new EventEmitter();
	menu: HTMLElement;
	toggle = false;

	constructor(private el: ElementRef, private renderer: Renderer2) { }

	/**
	 * @description Listens for click event on the button and then toggles
	 */
	@HostListener('click')
	onClick() {
		this.toggle = !this.toggle;
		this.toggleMenu();
	}

	/**
	 * @description Listens for click event on the whole document and removes the
	 * menu if it's visible and the click is outside the menu
	 * @param event of the click
	 */
	@HostListener('document:click', ['$event'])
	checkClickPosition(event) {
		const fileBtnClicked = event.target.id.indexOf('file-upload') >= 0;
		const convertBtnClicked = event.target.id.indexOf('file-convert') >= 0;

		if ((fileBtnClicked || convertBtnClicked) && this.menu) {
			this.removeMenu();
		}
	}

	/**
	 * @description Emits the selected format back to the host component
	 * @param format that has been selected
	 */
	emitFormat(format: IFormat) {
		this.selectFormat.emit(format);
	}

	/**
	 * @description Renders/removes the menu
	 */
	toggleMenu() {
		if (this.toggle) {
			this.menu = this.renderer.createElement('ul');

			for (const format of this.appSelectionMenu) {
				const listItem = this.renderer.createElement('li');

				this.renderer.appendChild(listItem, this.renderer.createText(format.name));
				this.renderer.addClass(listItem, 'selection-menu-item');
				this.renderer.listen(listItem, 'click', () => {
					this.emitFormat(format);
					this.removeMenu();
				});

				this.renderer.appendChild(this.menu, listItem);
			}

			this.renderer.addClass(this.menu, 'selection-menu');
			this.renderer.appendChild(document.body.getElementsByTagName('main')[0], this.menu);
			this.setPosition();
			setTimeout(() => this.renderer.addClass(this.menu, 'selection-menu-visible'));
		} else {
			this.removeMenu();
		}
	}

	/**
	 * @description Removes the menu from the DOM
	 */
	removeMenu() {
		this.renderer.removeClass(this.menu, 'selection-menu-visible');
		setTimeout(() => {
			this.renderer.removeChild(document.body.getElementsByTagName('main')[0], this.menu);
			this.toggle = false;
		}, 300);
	}

	/**
	 * @description Sets the position of the menu
	 */
	setPosition() {
		const hostPos = this.el.nativeElement.getBoundingClientRect();
		const menuPos = this.menu.getBoundingClientRect();

		this.renderer.setStyle(this.menu, 'top', `${(hostPos.top + 80)}px`);
		this.renderer.setStyle(this.menu, 'left', `${(hostPos.left + (hostPos.width - menuPos.width) / 2)}px`);
	}
}
