import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SelectionMenuDirective } from './directives/selection-menu.directive';
import { HttpClientModule } from '@angular/common/http';
import { FileSaverModule } from 'ngx-filesaver';

@NgModule({
	declarations: [
		AppComponent,
		SelectionMenuDirective
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		BrowserAnimationsModule,
		FormsModule,
		FileSaverModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
