import { ANIMATIONS } from './helpers/animations';
import { Component, OnInit } from '@angular/core';
import { BackEndService } from './services/back-end.service';
import { IFormat, IConversionProcess } from './helpers/types';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	animations: [ANIMATIONS.fade]
})
export class AppComponent implements OnInit {
	initialLoading = true;
	conversionProcess = IConversionProcess.IDLE;
	currentStep = 1;
	fileType: string;
	conversionUUID: string;

	allFormats: IFormat[];
	availableFormats: IFormat[];

	selectedFile: File;
	selectedFormat: IFormat;

	constructor(
		private backEndService: BackEndService
	) { }

	/**
	 * @description Initializes all necessary start-up data
	 */
	ngOnInit() {
		this.backEndService.getFormats().subscribe(formats => {
			this.allFormats = formats;
			this.initialLoading = false;
		});
	}

	/**
	 * @description handles selecting a file and setting values for keeping track of current progress
	 * @param file that has been uploaded
	 */
	selectFile(file: File) {
		this.conversionProcess = IConversionProcess.IDLE;

		if (file) {
			this.selectedFile = file;
			this.fileType = file.type.split('/')[0];
			this.availableFormats = this.backEndService.selectAvailableFormats(this.allFormats, this.selectedFile.type);

			this.currentStep = 2;
			this.selectedFormat = null;
		} else {
			this.currentStep = 1;
			this.selectedFile = null;
			this.selectedFormat = null;
		}
	}

	/**
	 * @description selects the format that the selected file will be converted to
	 * @param format that has been selected
	 */
	selectFormat(format) {
		this.selectedFormat = format;

		this.currentStep = 3;
		this.conversionProcess = IConversionProcess.IDLE;
	}

	/**
	 * @description converts the file and saves the UUID of the conversion
	 */
	convertFile() {
		this.conversionProcess = IConversionProcess.PROCESSING;
		this.backEndService.convertFile(this.selectedFile, this.selectedFormat).subscribe(uuid => {
			this.conversionProcess = IConversionProcess.DONE;
			setTimeout(() => {
				this.conversionUUID = uuid;
				this.conversionProcess = IConversionProcess.AVAILABLE;
			}, 750);
		});
	}

	/**
	 * @description downloads a file based on the UUID
	 */
	downloadFile() {
		const type = this.selectedFormat.name.toLowerCase();
		const mime = this.selectedFormat.type;
		this.backEndService.saveFile(this.conversionUUID, type, mime);
	}
}
